package com.tide.data.constants

object Const {

    const val RETROFIT_BASE_URL = "https://api.spacexdata.com/v4/"
    const val SHARED_PREFS_WELCOME_DIALOG_SHOWN_KEY = "shared_prefs_welcome_dialog_shown"
    const val ARG_ROCKET_ID = "arg_rocket_id"

}