package com.tide.data.di

import androidx.preference.PreferenceManager
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.tide.MyApplication
import com.tide.data.constants.Const
import com.tide.data.network.ApiHelper
import com.tide.data.network.ApiHelperImpl
import com.tide.data.network.ApiService
import com.tide.data.repository.MainRepository
import com.tide.ui.viewmodel.LaunchesViewModel
import com.tide.ui.viewmodel.MainActivityViewModel
import com.tide.ui.viewmodel.RocketsViewModel
import com.tide.utils.AppSharedPreferences
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val appModule = module {

    single { provideRetrofit() }
    single { provideApiService(get()) }
    single { provideApiHelper(get()) }
    single { MainRepository(get()) }
    single { provideAppSharedPreferences() }

    viewModel { MainActivityViewModel(get()) }
    viewModel { RocketsViewModel(get()) }
    viewModel { LaunchesViewModel(get()) }
}

fun provideAppSharedPreferences(): AppSharedPreferences {
    return AppSharedPreferences(PreferenceManager.getDefaultSharedPreferences(MyApplication.instance))
}

private fun provideRetrofit(): Retrofit {
    val gson = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    return Retrofit.Builder()
        .baseUrl(Const.RETROFIT_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
}

private fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

private fun provideApiHelper(apiService: ApiService): ApiHelper = ApiHelperImpl(apiService)
