package com.tide.data.model

data class Launches(val docs: List<Launch>)

data class Launch(val name: String, val dateUnix: Long, val dateLocal: String, val success: Boolean, val links: Links)

data class Links(val patch: Patch)

data class Patch(val small: String, val large: String)


