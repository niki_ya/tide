package com.tide.data.model

data class Rocket(val id: String,
                  val name: String,
                  val country: String,
                  val engines: Engine,
                  val flickrImages: List<String>,
                  val active: Boolean,
                  val description: String)

data class Engine(val number: Int)
