package com.tide.data.network

import com.tide.data.model.Launches
import com.tide.data.model.Rocket
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiHelper {

    suspend fun getRockets(): List<Rocket>
    suspend fun getLaunches(request: LaunchesRequest): Launches

}