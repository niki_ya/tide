package com.tide.data.network

import com.tide.data.model.Launches
import com.tide.data.model.Rocket
import retrofit2.http.Body

class ApiHelperImpl(private val apiService: ApiService): ApiHelper {

    override suspend fun getRockets(): List<Rocket> = apiService.getRockets()

    override suspend fun getLaunches(request: LaunchesRequest): Launches = apiService.getLaunches(request)

}