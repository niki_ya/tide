package com.tide.data.network

data class LaunchesRequest(val query: Query, val options: Options)