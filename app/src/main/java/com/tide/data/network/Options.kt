package com.tide.data.network

data class Options(val pagination: Boolean = false)