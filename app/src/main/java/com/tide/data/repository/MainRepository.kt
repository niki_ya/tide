package com.tide.data.repository

import com.tide.data.model.Launch
import com.tide.data.model.Launches
import com.tide.data.model.Rocket
import com.tide.data.network.*
import com.tide.utils.DateTimeUtils

class MainRepository(private val apiHelper: ApiHelper) {

    private val rockets = mutableListOf<Rocket>()
    private val launches = hashMapOf<String, Launches>()

    suspend fun getRockets(showOnlyActive: Boolean): List<Rocket> {
        rockets.clear()
        rockets.addAll(apiHelper.getRockets())

        return if (showOnlyActive) {
            getActiveRockets()
        } else {
            getAllRockets()
        }
    }

    suspend fun getLaunches(id: String): Launches {
        launches[id] = apiHelper.getLaunches(LaunchesRequest(Query(id), Options()))

        return launches[id]!!
    }

    fun getActiveRockets(): List<Rocket> {
        return rockets.filter { it.active }
    }

    fun getAllRockets(): List<Rocket> {
        return rockets.toMutableList()
    }

    fun getLaunchById(id: String): Launches? {
        return launches[id]
    }

    fun getRocketById(id: String): Rocket? {
        for (rocket in rockets) {
            if (rocket.id == id) {
                return rocket
            }
        }

        return null
    }

}