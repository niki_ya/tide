package com.tide.ui.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.navigation.fragment.findNavController
import com.tide.R
import com.tide.databinding.ActivityMainBinding
import com.tide.ui.viewmodel.MainActivityViewModel
import com.tide.utils.DialogUtils
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by viewModel()

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        viewModel.shouldShowWelcomeDialog().observe(this, { showDialog ->
            if (showDialog) {
                DialogUtils.showDialog(this,
                    getString(R.string.alert_dialog_welcome_title),
                    getString(R.string.alert_dialog_welcome_message),
                    getString(R.string.alert_dialog_welcome_positive_button)
                )
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}