package com.tide.ui.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tide.R
import com.tide.data.model.Launch
import com.tide.data.model.ListAdapterItem
import com.tide.databinding.ListItemLaunchBinding
import com.tide.databinding.ListItemLaunchYearBinding

class LaunchesAdapter:
    ListAdapter<ListAdapterItem, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TYPE_YEAR) {
            YearViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_launch_year, parent, false))
        } else {
            LaunchViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_launch, parent, false))
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder.itemViewType == TYPE_YEAR) {
            (viewHolder as YearViewHolder).bind(getItem(position).content.toString())
        } else {
            (viewHolder as LaunchViewHolder).bind(getItem(position).content as Launch)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position).content is String) {
            TYPE_YEAR
        } else {
            TYPE_LAUNCH
        }
    }

    class LaunchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ListItemLaunchBinding.bind(itemView)

        fun bind(launch: Launch) {
            with(itemView) {
                binding.launchName.text = launch.name
                binding.launchDate.text = launch.dateLocal
                binding.launchSuccess.text = context.getString(R.string.list_item_launch_success, launch.success.toString())

                Glide.with(this).load(launch.links.patch.large).into( binding.launchImageView)
            }
        }
    }

    class YearViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ListItemLaunchYearBinding.bind(itemView)

        fun bind(year: String) {
            binding.launchHeaderYear.text = year
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ListAdapterItem>() {
            override fun areItemsTheSame(oldItem: ListAdapterItem, newItem: ListAdapterItem): Boolean = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: ListAdapterItem, newItem: ListAdapterItem): Boolean = oldItem == newItem
        }

        const val TYPE_YEAR = 1
        const val TYPE_LAUNCH = 2
    }
}