package com.tide.ui.view.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tide.R
import com.tide.data.constants.Const
import com.tide.data.model.Rocket
import com.tide.databinding.ListItemRocketBinding

class RocketsAdapter:
    ListAdapter<Rocket, RocketsAdapter.RocketViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        RocketViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_rocket, parent, false))

    override fun onBindViewHolder(viewHolder: RocketViewHolder, position: Int) = viewHolder.bind(getItem(position))

    class RocketViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ListItemRocketBinding.bind(itemView)

        fun bind(rocket: Rocket) {
            with(itemView) {
                setOnClickListener {
                    val bundle = Bundle()

                    bundle.putString(Const.ARG_ROCKET_ID, rocket.id)

                    findNavController(it).navigate(R.id.action_rockets_to_launches_fragment, bundle)
                }
                binding.rocketName.text = rocket.name
                binding.rocketCountry.text = rocket.country
                binding.rocketEngines.text = context.getString(R.string.list_item_rocket_engines, rocket.engines.number)

                rocket.flickrImages.firstOrNull()?.let { url ->
                    Glide.with(this).load(url).into( binding.rocketImageView)
                }
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Rocket>() {
            override fun areItemsTheSame(oldItem: Rocket, newItem: Rocket): Boolean = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: Rocket, newItem: Rocket): Boolean = oldItem == newItem
        }
    }
}