package com.tide.ui.view.custom

import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.formatter.ValueFormatter

interface CustomLineChart {

    fun setDescriptionEnabled(enabled: Boolean)
    fun setAxisRightEnabled(enabled: Boolean)
    fun setLegendEnabled(enabled: Boolean)
    fun setTouchEnabled(enabled: Boolean)
    fun setDrawGridBackground(draw: Boolean)
    fun setXAxisPosition(position: XAxis.XAxisPosition)
    fun setData(data: List<Pair<Float, Float>>)
    fun setLineColor(color: Int)
    fun setLineWidth(width: Float)
    fun setCircleRadius(radius: Float)
    fun setCircleColor(color: Int)
    fun setDataTextSize(size: Float)
    fun setDrawCircleHole(draw: Boolean)
    fun setFormatAxis(formatter: ValueFormatter)
    fun setYAxisInterval(interval: Float)
    fun setXAxisInterval(interval: Float)
    fun setYMin(min: Float)
    fun setYMax(max: Float)

}