package com.tide.ui.view.custom

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import com.github.mikephil.charting.components.XAxis
import com.tide.utils.ChartValueFormatter

class LaunchesLineChart @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0):
    MPLineChart(context, attrs, defStyleAttr) {

    companion object {
        const val LINE_WIDTH = 1f
        const val DATA_CIRCLE_RADIUS = 3f
        const val DATA_TEXT_SIZE = 9f
        const val LINE_COLOR = Color.BLACK
        const val BACKGROUND_COLOR = Color.WHITE
        const val Y_AXIS_MIN = 0f
        const val Y_AXIS_INTERVAL = 1f
        const val X_AXIS_INTERVAL = 1f
    }

    init {
        setStyleAndOptions()
    }

    //TODO make into attributes so we can apply with xml
    private fun setStyleAndOptions() {
        setBackgroundColor(BACKGROUND_COLOR)
        setDescriptionEnabled(false)
        setAxisRightEnabled(false)
        setLegendEnabled(false)
        setTouchEnabled(false)
        setDrawGridBackground(false)
        setXAxisPosition(XAxis.XAxisPosition.BOTTOM)
        setLineColor(LINE_COLOR)
        setLineWidth(LINE_WIDTH)
        setCircleRadius(DATA_CIRCLE_RADIUS)
        setCircleColor(LINE_COLOR)
        setDataTextSize(DATA_TEXT_SIZE)
        setDrawCircleHole(false)
        setFormatAxis(ChartValueFormatter())
        setYAxisInterval(Y_AXIS_INTERVAL)
        setXAxisInterval(X_AXIS_INTERVAL)
        setYMin(Y_AXIS_MIN)
    }


}