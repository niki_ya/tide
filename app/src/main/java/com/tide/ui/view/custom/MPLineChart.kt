package com.tide.ui.view.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.tide.utils.ChartValueFormatter
import java.text.NumberFormat
import java.util.*


open class MPLineChart @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0):
    RelativeLayout(context, attrs, defStyleAttr), CustomLineChart {

    private var chart: LineChart = LineChart(context)
    private val lineSet = LineDataSet(listOf(), "Label")

    init {
        val dataSets = listOf<ILineDataSet>(lineSet)

        chart.data = LineData(dataSets)

        chart.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)

        addView(chart)
    }

    override fun setData(data: List<Pair<Float, Float>>) {
        if (chart.data != null && chart.data.dataSetCount > 0) {
            val lineSet = chart.data.getDataSetByIndex(0) as LineDataSet

            lineSet.values = getEntryList(data)

            notifyDataChanged()
            chart.invalidate()
        }
    }

    private fun notifyDataChanged() {
        val lineSet = chart.data?.getDataSetByIndex(0) as LineDataSet?

        lineSet?.notifyDataSetChanged()
        chart.data?.notifyDataChanged()
        chart.notifyDataSetChanged()
    }

    private fun getEntryList(data: List<Pair<Float, Float>>): List<Entry> {
        val values = ArrayList<Entry>()

        for (pair in data) {
            values.add(Entry(pair.first, pair.second))
        }

        return values
    }

    override fun setDescriptionEnabled(enabled: Boolean) {
        chart.description.isEnabled = enabled
    }

    override fun setAxisRightEnabled(enabled: Boolean) {
        chart.axisRight.isEnabled = enabled
    }

    override fun setLegendEnabled(enabled: Boolean) {
        chart.legend.isEnabled = enabled
    }

    override fun setTouchEnabled(enabled: Boolean) {
        chart.setTouchEnabled(enabled)
    }

    override fun setDrawGridBackground(draw: Boolean) {
        chart.setDrawGridBackground(draw)
    }

    override fun setXAxisPosition(position: XAxis.XAxisPosition) {
        chart.xAxis.position = position
    }

    override fun setLineColor(color: Int) {
        lineSet.color = color
    }

    override fun setLineWidth(width: Float) {
        lineSet.lineWidth = width
    }

    override fun setCircleRadius(radius: Float) {
        lineSet.circleRadius = radius
    }

    override fun setCircleColor(color: Int) {
        lineSet.setCircleColor(color)
    }

    override fun setDataTextSize(size: Float) {
        lineSet.valueTextSize = size
    }

    override fun setDrawCircleHole(draw: Boolean) {
        lineSet.setDrawCircleHole(draw)
    }

    override fun setFormatAxis(formatter: ValueFormatter) {
        chart.axisLeft.valueFormatter = formatter
        chart.xAxis.valueFormatter = formatter
    }

    override fun setYAxisInterval(interval: Float) {
        chart.axisLeft.granularity = interval
    }

    override fun setXAxisInterval(interval: Float) {
        chart.xAxis.granularity = interval
    }

    override fun setYMin(min: Float) {
        chart.axisLeft.axisMinimum = min
    }

    override fun setYMax(max: Float) {
        chart.axisLeft.axisMaximum = max
    }

}