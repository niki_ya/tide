package com.tide.ui.view.fragment

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.tide.R
import com.tide.data.model.Launch
import com.tide.data.model.ListAdapterItem
import com.tide.databinding.FragmentLaunchesBinding
import com.tide.ui.view.adapter.LaunchesAdapter
import com.tide.ui.view.adapter.RocketsAdapter
import com.tide.ui.viewmodel.LaunchesViewModel
import com.tide.utils.DialogUtils
import com.tide.utils.Status
import org.koin.androidx.viewmodel.ext.android.viewModel


class LaunchesFragment: Fragment() {

    private val viewModel: LaunchesViewModel by viewModel()

    private var _binding: FragmentLaunchesBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: LaunchesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (activity as AppCompatActivity?)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLaunchesBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()

        viewModel.launchesLiveData.observe(viewLifecycleOwner, {
            it?.let { viewState ->
                when (viewState.status) {
                    Status.SUCCESS -> {
                        binding.launchesLoadingIndicator.visibility = View.GONE

                        adapter.submitList(viewState.data)
                    }
                    Status.ERROR -> {
                        binding.launchesLoadingIndicator.visibility = View.GONE

                        DialogUtils.showDialog(
                            requireContext(),
                            getString(R.string.alert_dialog_error_title),
                            viewState.message?: "",
                            getString(R.string.alert_dialog_error_positive_button)
                        )
                    }
                    Status.LOADING -> {
                        binding.launchesLoadingIndicator.visibility = View.VISIBLE
                    }
                }
            }
        })

        viewModel.chartLiveData.observe(viewLifecycleOwner, {
            it?.let { data ->
                binding.launchesChart.setData(data)
            }
        })

        viewModel.descriptionLiveData.observe(viewLifecycleOwner, {
            it?.let { text ->
                binding.launchesRocketDescription.text = text
            }
        })

        viewModel.fetchLaunches(arguments)

    }

    private fun setupRecyclerView() {
        adapter = LaunchesAdapter()
        binding.launchesRecyclerView.adapter = adapter
        binding.launchesRecyclerView.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onDestroy() {
        super.onDestroy()

        (activity as AppCompatActivity?)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }
}