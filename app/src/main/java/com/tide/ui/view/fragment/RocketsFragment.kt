package com.tide.ui.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.whenStarted
import androidx.recyclerview.widget.LinearLayoutManager
import com.tide.R
import com.tide.databinding.FragmentRocketsBinding
import com.tide.ui.view.adapter.RocketsAdapter
import com.tide.ui.viewmodel.RocketsViewModel
import com.tide.utils.DialogUtils
import com.tide.utils.Status
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class RocketsFragment: Fragment() {

    private val viewModel: RocketsViewModel by viewModel()

    private var _binding: FragmentRocketsBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: RocketsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRocketsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()

        binding.rocketsRefreshLayout.setOnRefreshListener {
            viewModel.fetchRockets()
        }

        viewModel.rocketsLiveData.observe(viewLifecycleOwner, {
            it?.let { viewState ->
                when (viewState.status) {
                    Status.SUCCESS -> {
                        binding.rocketsRefreshLayout.isRefreshing = false

                        viewState.data?.let { data ->
                            adapter.submitList(data)
                        }
                    }
                    Status.ERROR -> {
                        binding.rocketsRefreshLayout.isRefreshing = false

                        DialogUtils.showDialog(
                            requireContext(),
                            getString(R.string.alert_dialog_error_title),
                            viewState.message?: "",
                            getString(R.string.alert_dialog_error_positive_button)
                        )
                    }
                    Status.LOADING -> {
                        binding.rocketsRefreshLayout.isRefreshing = true
                    }
                }
            }
        })

        viewModel.fetchRockets()
    }

    private fun setupRecyclerView() {
        adapter = RocketsAdapter()
        binding.rocketsRecyclerView.adapter = adapter
        binding.rocketsRecyclerView.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.rockets_menu, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.menu_show_active).isVisible = !viewModel.showOnlyActive
        menu.findItem(R.id.menu_show_all).isVisible = viewModel.showOnlyActive

        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_refresh -> {
                viewModel.fetchRockets()
                return true
            }
            R.id.menu_show_active -> {
                viewModel.showOnlyActive()
                return true
            }
            R.id.menu_show_all -> {
                viewModel.showAllRockets()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}