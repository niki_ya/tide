package com.tide.ui.viewmodel

import android.os.Bundle
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tide.data.constants.Const
import com.tide.data.model.Launches
import com.tide.data.model.ListAdapterItem
import com.tide.data.model.Rocket
import com.tide.data.repository.MainRepository
import com.tide.utils.DateTimeUtils
import com.tide.utils.ViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LaunchesViewModel(private val mainRepository: MainRepository): ViewModel() {

    private val _launchesLiveData = MutableLiveData<ViewState<List<ListAdapterItem>>>()
    val launchesLiveData: LiveData<ViewState<List<ListAdapterItem>>> get() = _launchesLiveData

    private val _chartLiveData = MutableLiveData<List<Pair<Float, Float>>>()
    val chartLiveData: LiveData<List<Pair<Float, Float>>> get() = _chartLiveData

    private val _descriptionLiveData = MutableLiveData<String>()
    val descriptionLiveData: LiveData<String> get() = _descriptionLiveData

    private var rocketId: String? = null

    fun fetchLaunches(arguments: Bundle?) {
        arguments?.let { bundle ->
            rocketId = bundle.getString(Const.ARG_ROCKET_ID)

            rocketId?.let { id ->
                _descriptionLiveData.value = mainRepository.getRocketById(id)?.description

                viewModelScope.launch(Dispatchers.IO) {
                    _launchesLiveData.postValue(ViewState.loading())
                    try {
                        mainRepository.getLaunches(id)

                        withContext(Dispatchers.Main) {
                            sendData()
                        }
                    } catch (exception: Exception) {
                        _launchesLiveData.postValue(ViewState.error(null, exception.message ?: "Error Occurred!"))
                    }
                }
            }
        }
    }

    private fun sendData() {
        rocketId?.let { id ->
            val launch = mainRepository.getLaunchById(id)

            launch?.let {

                val chartData =  transformLaunchesToChartData(it)

                _chartLiveData.value = chartData
                _launchesLiveData.value = ViewState.success(getListAdapterItems(it))
            }
        }
    }

    fun getListAdapterItems(fromLaunches: Launches): List<ListAdapterItem> {
        val listItems = ArrayList<ListAdapterItem>()

        for (doc in fromLaunches.docs) {
            val year = DateTimeUtils.getYear(doc.dateUnix)

            if (!listItems.contains(ListAdapterItem(year.toString(), year.toString()))) {
                listItems.add(ListAdapterItem(year.toString(), year.toString()))
                listItems.add(ListAdapterItem(doc.name, doc))
            } else {
                listItems.add(ListAdapterItem(doc.name, doc))
            }
        }

        return listItems
    }

    fun transformLaunchesToChartData(fromLaunches: Launches): List<Pair<Float, Float>> {
        val data = mutableListOf<Pair<Float, Float>>()
        val dataMap = hashMapOf<Float, Float>()

        for (doc in fromLaunches.docs) {
            val year = DateTimeUtils.getYear(doc.dateUnix).toFloat()

            if (dataMap[year] != null) {
                val value = dataMap[year]!! + 1

                dataMap[year] = value
            } else {
                dataMap[year] = 1f
            }
        }

        for ((year, launches) in dataMap) {
            data.add(Pair(year, launches))
        }

        return data
    }
}