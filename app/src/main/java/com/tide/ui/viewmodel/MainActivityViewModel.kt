package com.tide.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.tide.data.constants.Const.SHARED_PREFS_WELCOME_DIALOG_SHOWN_KEY
import com.tide.utils.AppSharedPreferences

class MainActivityViewModel(private val sharedPreferences: AppSharedPreferences): ViewModel() {

    fun shouldShowWelcomeDialog() = liveData {
        val dialogShownBefore = sharedPreferences.getBoolean(SHARED_PREFS_WELCOME_DIALOG_SHOWN_KEY, false)

        if (!dialogShownBefore) {
            sharedPreferences.edit().putBoolean(SHARED_PREFS_WELCOME_DIALOG_SHOWN_KEY, true).apply()

            emit(true)
        } else {
            emit(false)
        }
    }

}