package com.tide.ui.viewmodel

import androidx.lifecycle.*
import com.tide.data.model.Rocket
import com.tide.data.repository.MainRepository
import com.tide.utils.ViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RocketsViewModel(private val mainRepository: MainRepository): ViewModel() {

    private val _rocketsLiveData = MutableLiveData<ViewState<List<Rocket>>>()
    val rocketsLiveData: LiveData<ViewState<List<Rocket>>> get() = _rocketsLiveData

    var showOnlyActive = false
        private set

    fun fetchRockets() {
        viewModelScope.launch(Dispatchers.IO) {
            _rocketsLiveData.postValue(ViewState.loading())
            try {
                _rocketsLiveData.postValue(ViewState.success(mainRepository.getRockets(showOnlyActive)))
            } catch (exception: Exception) {
                _rocketsLiveData.postValue(ViewState.error(null, exception.message ?: "Error Occurred!"))
            }
        }
    }

    fun showOnlyActive() {
        showOnlyActive = true

        _rocketsLiveData.value = ViewState.success(mainRepository.getActiveRockets())
    }

    fun showAllRockets() {
        showOnlyActive = false

        _rocketsLiveData.value = ViewState.success(mainRepository.getAllRockets())
    }



}