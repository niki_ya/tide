package com.tide.utils

import android.content.SharedPreferences

class AppSharedPreferences(private val sharedPreferences: SharedPreferences) {

    fun getBoolean(key: String, defValue: Boolean): Boolean {
        return sharedPreferences.getBoolean(key, defValue)
    }

    fun edit(): SharedPreferences.Editor {
        return sharedPreferences.edit()
    }
}