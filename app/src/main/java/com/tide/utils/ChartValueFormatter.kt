package com.tide.utils

import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.NumberFormat
import java.util.*

class ChartValueFormatter: ValueFormatter() {
    private var formatter: NumberFormat = NumberFormat.getNumberInstance(Locale.US)

    init {
        formatter.maximumFractionDigits = 0
        formatter.isGroupingUsed = false
    }

    override fun getFormattedValue(value: Float): String {
        return formatter.format(value)
    }
}