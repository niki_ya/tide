package com.tide.utils

import java.util.*

object DateTimeUtils {

    fun getYear(unix: Long): Int {
        val cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"))

        cal.timeInMillis = unix * 1000

        return cal.get(Calendar.YEAR)
    }

}