package com.tide.utils

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import com.tide.R

object DialogUtils {

    fun showDialog(context: Context, title: String, message: String, buttonText: String) {
        AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message).setPositiveButton(buttonText, { dialog, i -> dialog.dismiss() })
            .show()
    }

}