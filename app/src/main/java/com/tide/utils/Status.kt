package com.tide.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}