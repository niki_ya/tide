package com.tide.data.repository

import com.tide.data.constants.Const
import com.tide.data.model.Launches
import com.tide.data.model.Rocket
import com.tide.data.network.ApiHelper
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class MainRepositoryTest {

    private val api = mock<ApiHelper> {
        onBlocking { getRockets() } doReturn listOf(Rocket("id", "name", "country", mock(), mock(), true, "description"))
    }
    private val repo = MainRepository(api)

    @Test
    fun getActiveRocketsRemotelyActiveTrueTest() {
        runBlocking {
            val result = repo.getRockets(true)

            assertTrue(result.isNotEmpty())
            assertEquals(result.size, 1)
            assertEquals("id", result[0].id)
            assertEquals("name", result[0].name)
        }
    }

}