package com.tide.ui.viewmodel

import com.tide.data.model.Launch
import com.tide.data.model.Launches
import com.tide.data.model.Links
import com.tide.data.repository.MainRepository
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.mock

class LaunchesViewModelTest {

    private var mainRepo = mock<MainRepository>()
    private var viewModel = LaunchesViewModel(mainRepo)


    @Test
    fun getListAdapterItemsTest() {
        val launches = Launches(listOf(Launch("name", 1626650411L, "date", false, mock())))
        val result = viewModel.getListAdapterItems(launches)

        assertNotNull(result)
        assertEquals(result.size, 2)
        assertEquals("2021", result[0].id)
        assertEquals("name", result[1].id)
        assertNotNull("name", result[1].content)
        assertEquals("name", (result[1].content as Launch).name)
        assertEquals(1626650411L, (result[1].content as Launch).dateUnix)
        assertEquals("date", (result[1].content as Launch).dateLocal)
        assertEquals(false, (result[1].content as Launch).success)
    }

    @Test
    fun transformLaunchesToChartDataTest() {
        val launches = Launches(
            listOf(Launch("name", 1626650411L, "date", false, mock()),
                Launch("name2", 162665000L, "date", false, mock()),
                Launch("name3", 162665000L, "date", false, mock())))

        val result = viewModel.transformLaunchesToChartData(launches)

        assertNotNull(result)
        assertEquals(result.size, 2)
        assertEquals(2021f, result[1].first)//expected year
        assertEquals(1f, result[1].second)//expected elements
        assertEquals(1975f, result[0].first)//expected year
        assertEquals(2f, result[0].second)//expected elements
    }

}