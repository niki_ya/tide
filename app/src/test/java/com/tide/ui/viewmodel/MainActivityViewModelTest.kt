package com.tide.ui.viewmodel

import android.content.SharedPreferences
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.tide.data.constants.Const.SHARED_PREFS_WELCOME_DIALOG_SHOWN_KEY
import com.tide.utils.AppSharedPreferences
import com.tide.utils.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.*
import org.junit.Assert.assertEquals
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock


class MainActivityViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var appSharedPreferences: AppSharedPreferences
    private lateinit var viewModel: MainActivityViewModel

    @Test
    fun whenSharedPrefsWelcomeShown_shouldShowWelcomeDialog_false() {
        appSharedPreferences = mock {
            on { getBoolean(SHARED_PREFS_WELCOME_DIALOG_SHOWN_KEY, false) } doReturn true
        }

        viewModel = MainActivityViewModel(appSharedPreferences)

        val result = viewModel.shouldShowWelcomeDialog().getOrAwaitValue()

        assertEquals(false, result)
    }
}